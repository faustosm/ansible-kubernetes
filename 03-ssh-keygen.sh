#!/bin/bash

# Gerar um par de chaves na sua máquina

ssh-keygen

# Pressione ENTERpara salvar o par de chaves no /root/.ssh

# Acesse o diretorio abaixo pra verificar as chaves criadas

 cd /root/.ssh

#Copie sua chave publica para TODOS servidores inclusive o Master

ssh-copy-id root@10.128.0.17

ssh-copy-id root@10.128.0.20

ssh-copy-id root@10.128.0.21