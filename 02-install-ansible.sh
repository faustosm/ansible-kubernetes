    #!/bin/bash

    sudo yum update -y && yum install nano ansible git -y

    # Crie um diretorio para servir como area de trabalho

    sudo mkdir ~/cluster-mapfre

    # Entre no diretorio criado

    cd ~/cluster-mapfre

    # Criando arquivos de configuração

    sudo touch hosts 04-create-user.yml 05-disable-firwalld 06-disable-swap.yml 07-install-packages.yml 08-kube-dependencies.yml 09-master.yml 10-workers.yml

# Crie o arquivo hosts com as maquinas do cluster

nano ~/cluster-mapfre/hosts

[masters]
master ansible_host=10.128.0.17 ansible_user=root

[workers]
worker1 ansible_host=10.128.0.20 ansible_user=root
worker2 ansible_host=10.128.0.21 ansible_user=root

# Criar usuários não root

ansible-playbook -i hosts 04-create-user.yml

# Desabilita firwalld

nano 05-disable-firewall.yml

ansible-playbook -i hosts 05-disable-firwalld

# Desabilitando SWAP

nano 06-disable-swap.yml

ansible-playbook -i hosts 06-disable-swap.yml

# Instalando pacotes basicos CentOS

nano 07-install-packages.yml

ansible-playbook -i hosts 07-install-packages.yml

# Instalando dependencias do Kubernetes

nano 08-kube-dependencies.yml

ansible-playbook -i hosts 08-kube-dependencies.yml

# Configurando O Master

nano 09-master.yml

ansible-playbook -i hosts 09-master.yml

# Configurando o Node

nano 10-workers.yml

ansible-playbook -i hosts 10-workers.yml